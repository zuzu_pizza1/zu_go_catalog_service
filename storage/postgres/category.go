package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"market6405216/market_go_catalog_service/genproto/catalog_service"
	"market6405216/market_go_catalog_service/pkg/helper"
	"market6405216/market_go_catalog_service/storage"
)

type CategoryRepo struct {
	db *pgxpool.Pool
}

func NewCategoryRepo(db *pgxpool.Pool) storage.CategoryRepoI {
	return &CategoryRepo{
		db: db,
	}
}

func (c *CategoryRepo) Create(ctx context.Context, req *catalog_service.CreateCategory) (resp *catalog_service.CategoryPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "categories" (
				category_id,
				category_name,
				updated_at
			) VALUES ($1, $2, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Name,
	)

	if err != nil {
		return nil, err
	}

	return &catalog_service.CategoryPrimaryKey{CategoryId: id.String()}, nil
}

func (c *CategoryRepo) GetByPKey(ctx context.Context, req *catalog_service.CategoryPrimaryKey) (resp *catalog_service.Category, err error) {

	query := `
		SELECT
			category_id,
			category_name,
			created_at,
			updated_at
		FROM "categories"
		where category_id = $1
	`

	var (
		category_id   sql.NullString
		category_name sql.NullString
		createdAt     sql.NullString
		updatedAt     sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.GetCategoryId()).Scan(
		&category_id,
		&category_name,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &catalog_service.Category{
		CategoryId: category_id.String,
		Name:       category_name.String,
		CreatedAt:  createdAt.String,
		UpdatedAt:  updatedAt.String,
	}

	return
}

func (c *CategoryRepo) GetAll(ctx context.Context, req *catalog_service.GetListCategoryRequest) (resp *catalog_service.GetListCategoryResponse, err error) {

	resp = &catalog_service.GetListCategoryResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			category_id,
			category_name,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "categories"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var category catalog_service.Category

		err := rows.Scan(
			&resp.Count,
			&category.CategoryId,
			&category.Name,
			&category.CreatedAt,
			&category.UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Categories = append(resp.Categories, &category)
	}

	return
}

func (c *CategoryRepo) Update(ctx context.Context, req *catalog_service.UpdateCategory) (rowsAffected int64, err error) {

	query := `
			UPDATE
			    "categories"
			SET
				category_id = $1,
				category_name = $2,
				updated_at = now()
			WHERE
			category_id = $3`

	result, err := c.db.Exec(ctx, query,
		req.CategoryId,
		req.Name,
		req.CategoryId,
	)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *CategoryRepo) Delete(ctx context.Context, req *catalog_service.CategoryPrimaryKey) error {

	query := `DELETE FROM "categories" WHERE category_id = $1`

	// query := `UPDATE "categories" SET deleted_at = now() WHERE category_id = $1`

	_, err := c.db.Exec(ctx, query, req.CategoryId)

	if err != nil {
		return err
	}

	return nil
}
