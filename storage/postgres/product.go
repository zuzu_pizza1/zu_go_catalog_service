package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"market6405216/market_go_catalog_service/genproto/catalog_service"
	"market6405216/market_go_catalog_service/pkg/helper"
	"market6405216/market_go_catalog_service/storage"
)

type ProductRepo struct {
	db *pgxpool.Pool
}

func NewProductRepo(db *pgxpool.Pool) storage.ProductRepoI {
	return &ProductRepo{
		db: db,
	}
}

func (c *ProductRepo) Create(ctx context.Context, req *catalog_service.CreateProduct) (resp *catalog_service.ProductPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "products" (
				product_id,
				product_name,
				img,
				description,
				price,
				category_id,
				updated_at
			) VALUES ($1, $2, $3, $4, $5, $6, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.ProductName,
		req.Img,
		req.Description,
		req.Price,
		req.CategoryId,
	)

	if err != nil {
		return nil, err
	}

	return &catalog_service.ProductPrimaryKey{ProductId: id.String()}, nil
}

func (c *ProductRepo) GetByPKey(ctx context.Context, req *catalog_service.ProductPrimaryKey) (resp *catalog_service.Product, err error) {

	query := `
		SELECT
			product_id,
			product_name,
			img,
			description,
			price,
			category_id,
			created_at,
			updated_at
		FROM "products"
		WHERE product_id = $1 
	`

	var (
		product_id   sql.NullString
		product_name sql.NullString
		img          sql.NullString
		description  sql.NullString
		price        sql.NullFloat64
		category_id  sql.NullString
		createdAt    sql.NullString
		updatedAt    sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.GetProductId()).Scan(
		&product_id,
		&product_name,
		&img,
		&description,
		&price,
		&category_id,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &catalog_service.Product{
		ProductId:   product_id.String,
		ProductName: product_name.String,
		Img:         img.String,
		Description: description.String,
		Price:       price.Float64,
		CategoryId:  category_id.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *ProductRepo) GetAll(ctx context.Context, req *catalog_service.GetListProductRequest) (resp *catalog_service.GetListProductResponse, err error) {

	resp = &catalog_service.GetListProductResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	var category_id sql.NullString

	query = `
		SELECT
			COUNT(*) OVER(),
			product_id,
		
			product_name,
			img,
			description,
			COALESCE(price, 0),
			category_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "products"
		
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.Search) > 0 {
		filter = " AND category_name ILIKE " + "'%" + req.Search + "%'"
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {

		var product catalog_service.Product

		err := rows.Scan(
			&resp.Count,
			&product.ProductId,
			&product.ProductName,
			&product.Img,
			&product.Description,
			&product.Price,
			&category_id,
			&product.CreatedAt,
			&product.UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		product.CategoryId = category_id.String

		resp.Products = append(resp.Products, &product)
	}

	return
}

func (c *ProductRepo) Update(ctx context.Context, req *catalog_service.UpdateProduct) (rowsAffected int64, err error) {

	query := `
			UPDATE
			    "products"
			SET
				product_id = $1,
				product_name = $2,
				img = $3,
				description = $4,
				price = $5,
				category_id = $6,
				updated_at = now()
			WHERE
				product_id = $7`

	result, err := c.db.Exec(ctx, query,
		req.ProductId,
		req.GetProductName(),
		req.GetImg(),
		req.GetDescription(),
		req.GetPrice(),
		req.GetCategoryId(),
		req.ProductId,
	)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *ProductRepo) Delete(ctx context.Context, req *catalog_service.ProductPrimaryKey) error {

	query := `DELETE FROM "products" WHERE product_id = $1`

	// query := `UPDATE "products" SET deleted_at = now() WHERE product_id = $1`

	_, err := c.db.Exec(ctx, query, req.GetProductId())

	if err != nil {
		return err
	}

	return nil
}
