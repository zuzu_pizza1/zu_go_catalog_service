
CREATE TABLE IF NOT EXISTS "categories" (
    "category_id" UUID PRIMARY KEY,
    "category_name" VARCHAR(50) NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);


CREATE TABLE IF NOT EXISTS "products" (
    "product_id" UUID PRIMARY KEY,
    "product_name" VARCHAR(50) NOT NULL,
    "img" VARCHAR(50)   NOT NULL,
    "description" VARCHAR(50)   NOT NULL,
    "price" NUMERIC,
    "category_id" UUID REFERENCES categories (category_id),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);



SELECT
	COUNT(*) OVER(),
	product_id,
	barcode,
	product_name,
	COALESCE(price, 0),
	category_id,
	TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
	TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS'),
	COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')
FROM "products"
WHERE COALESCE(TO_CHAR(deleted_at, 'YYYY-MM-DD HH24:MI:SS'), '')=''
AND TRUE ORDER BY created_at DESC OFFSET 0 
